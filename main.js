const electron = require("electron");

const app = electron.app;

const BrowserWindow = electron.BrowserWindow;

let mainWindow;

require("./server");

function createWindow(){
  mainWindow = new BrowserWindow({
    width: 400,
    height:600
  });
  mainWindow.loadURL("http://localhost:8080/rooms/Elg8R2mb7LaFRl2sPjNmvqrAzq52izuaYVzlEvaVGNMv85QVuO9CdhM2");

  mainWindow.webContents.openDevTools();

  mainWindow.on("closed",function(){
    mainWindow = null;
  });
}

app.on("ready",createWindow);


// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})